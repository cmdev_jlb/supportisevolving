# JLB Support Theme

Hey team, this template can be used to create all of the landing pages! It allows for the use of Sass and the latest ES6 scripts syntax. It compiles and minifies the CSS and JavaScript for you and allows for auto-updating.

### Prerequisites

`node -v` && `npm -v` && `npm install gulp -g`

### Startup

1. download and open git bash
2. `git clone https://cmdev_jlb@bitbucket.org/cmdev_jlb/supportisevolving.git *projectName*/`
3. `cd *projectName*/`
4. `npm init -y && npm install gulp && npm install gulp-sass gulp-autoprefixer browser-sync gulp-eslint gulp-concat gulp-uglify gulp-babel babel-core babel-preset-env gulp-rename`
5. `gulp`
